# WGMODES.jl

## About

This is a Julia port of
[WGMODES](https://photonics.umd.edu/software/wgmodes/).
The original code is found at
[http://photonics.umd.edu/wp-content/uploads/software/wgmodes/modesolver-2011-04-22.zip](http://photonics.umd.edu/wp-content/uploads/software/wgmodes/modesolver-2011-04-22.zip)
and
[https://www.mathworks.com/matlabcentral/fileexchange/12734-waveguide-mode-solver](https://www.mathworks.com/matlabcentral/fileexchange/12734-waveguide-mode-solver),
and it has been mirrored to Gitlab at
[WGMODES](https://gitlab.com/pawelstrzebonski/WGMODES).

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/WGMODES.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for WGMODES.jl](https://pawelstrzebonski.gitlab.io/WGMODES.jl/).
