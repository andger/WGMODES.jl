using Documenter
import WGMODES

makedocs(
    sitename = "WGMODES.jl",
    pages = [
        "Home" => "index.md",
        "Example Usage" => "examples.md",
        "svmodes" => "svmodes.md",
        "wgmodes" => "wgmodes.md",
        "Code Validation" => "validation.md",
    ],
)
