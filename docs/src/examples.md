# Example Usage

## svmodes

Let's try to simulate the scalar modes of a fiber waveguide. For this
example we'll use the `svmodes` function. We begin by importing the
`WGMODES` package, as well as the `Plots` package that we'll use to
visualize the resulting modes. The inputs do not have to be in a particular
unit, so long as the units are consistent across the arguments. For example,
here we take the wavelength `lambda` to be 1.55 microns, and the grid
spacing to be 0.1 microns (100 nanometers). We define the index structure
as a two dimensional array. `WGMODES` takes the relative permittivity as
inputs, so we square the refractive index to obtain `epsilon`. The
mode solver will only determine a fixed number of modes, specified by
`nmodes` (in this case 3), whose modal effective indices are closest to the value
specified by `guess`. A good guess for index-guided structures is the
the core index values (in this case the maximal index value). The boundary
conditions on the four edges of the simulation domain are specified
in `boundary` as either zero-field ("0"), symmetric field ("S"), or
antisymmetric field ("A"). Finally, we specify what kind of modal field
we want to solve for ("scalar" field, or "EX" or "EY").

```@example svmodes
import WGMODES: svmodes
import Plots: plot, heatmap
import Plots: savefig # hide

lambda=1.55
dx=dy=0.1
x=y=-10:dx:10
index=[sqrt(x^2+y^2)<4 ? 1.46 : 1.45 for x=x, y=y]
epsilon=index.^2
guess=maximum(index)
nmodes=3
boundary="0000"
field="scalar"
nothing # hide
```

We then run the `svmodes` function, which will return the modal fields
`phi` as a 3D array (indexed as [x, y, mode #]), and the modal effective
indices `neff`.

```@example svmodes
phi, neff=svmodes(lambda, guess, nmodes, dx, dy, epsilon, boundary, field);
nothing # hide
```

We determine that the modal effective indices are approximately 1.4561,
1.4509, and 1.4509. We now plot the waveguide index structure and the
three modal fields

```@example svmodes
plot(
	heatmap(x, y, index,
		title="Waveguide Index Structure",
		aspectratio=1,
	),
	heatmap(x, y, phi[:,:,1],
		title="Waveguide Mode",
		aspectratio=1,
	),
	heatmap(x, y, phi[:,:,2],
		title="Waveguide Mode",
		aspectratio=1,
	),
	heatmap(x, y, phi[:,:,3],
		title="Waveguide Mode",
		aspectratio=1,
	),
	size=(800, 800),
)
savefig("svmodes_fiber.svg"); nothing # hide
```

![Plot of fiber waveguide and modes](svmodes_fiber.svg)

## wgmodes

Let's try to simulate the vectorial modes of a rib waveguide. For this
example we'll use the `wgmodes` function. We begin by importing the
`WGMODES` package, as well as the `Plots` package that we'll use to
visualize the resulting modes. The inputs do not have to be in a particular
unit, so long as the units are consistent across the arguments. For example,
here we take the wavelength `lambda` to be 1.55 microns, and the grid
spacing to be 0.02 microns (20 nanometers). We define the index structure
as a two dimensional array. `WGMODES` takes the relative permittivity as
inputs, so we square the refractive index to obtain `epsilon`. If
we desired structure is anisotropic, then the various components of the
permittivity are given as separate arrays (consult the function
documentation for more information). The mode solver will only determine
a fixed number of modes, specified by
`nmodes` (in this case 1), whose modal effective indices are closest to the value
specified by `guess`. A good guess for index-guided structures is the
the core index values (in this case the maximal index value). The boundary
conditions on the four edges of the simulation domain are specified
in `boundary` as either zero-field ("0"), symmetric field ("S"), or
antisymmetric field ("A").

```@example wgmodes
import WGMODES: wgmodes
import Plots: plot, heatmap
import Plots: savefig # hide

lambda=1.55
dx=dy=0.02
x=-2:dx:2
y=-1:dy:1
index=[y<0 ? 1.5 : (y<0.2 || (y<0.5 && abs(x)<1) ? 3.45 : 1.0) for y=y, x=x]
epsilon=index.^2
guess=maximum(index)
nmodes=1
boundary="0000"
nothing # hide
```

We now evaluate the modesolver to obtain the modal H field components in
the x and y directions, `hx` and `hy`, and the modal effective indices
`neff`.

```@example wgmodes
hx, hy, neff=wgmodes(lambda, guess, nmodes, dx, dy, epsilon, boundary);
nothing # hide
```

We now plot the waveguide structure as well as the modal field (total
as well as per component). As the output modal fields are one grid point
larger in every direction than the input permittivity array, we add
an additional point to `x` and `y` to ensure that we can plot the results
and define the axes values.

```@example wgmodes
x=[x; x[end]+dx]
y=[y; y[end]+dy]
plot(
	heatmap(x, y, index,
		title="Waveguide Index Structure",
		aspectratio=1,
	),
	heatmap(x, y, real.(hx[:,:,1].+hy[:,:,1]),
		title="Waveguide Mode (Total H Field)",
		aspectratio=1,
	),
	heatmap(x, y, real.(hx[:,:,1]),
		title="Waveguide Mode (H_x)",
		aspectratio=1,
	),
	heatmap(x, y, real.(hy[:,:,1]),
		title="Waveguide Mode (H_y)",
		aspectratio=1,
	),
	size=(800, 600),
)
savefig("wgmodes_rib.svg"); nothing # hide
```

![Plot of rib waveguide and mode](wgmodes_rib.svg)
