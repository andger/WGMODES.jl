# WGMODES.jl Documentation

`WGMODES.jl` is a Julia port of the Matlab utility
[WGMODES](https://photonics.umd.edu/software/wgmodes/), implementing
a finite difference mode solver described in
[“Vector Finite Difference Modesolver for Anisotropic Dielectric Waveguides"](https://doi.org/10.1109/JLT.2008.923643).
`WGMODES` is a finite difference mode solver that supports scalar,
semi- and fully vectorial mode solving on uniform and non-uniform
grids.

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/WGMODES.jl
```

Examples of calculating and plotting the waveguide modes
can be found at [Example Usage](@ref).

The code is in large part a relatively straight translation from
Matlab so while it may be (most likely) functional, it is not necessarily
in accordance to Julia best practices or optimal.

Current status:

* `svmodes` function has been implemented
* `wgmodes` function has been implemented
* Preliminary [Code Validation](@ref) indicates results are largely equivalent to original code
