# svmodes

The `svmodes` function is a port of the `svmodes.m` file from
[WGMODES](https://photonics.umd.edu/software/wgmodes/).
It is a semivectorial finite difference modesolver. Please consult
the function documentation for a list of arguments.

## Functions

```@docs
WGMODES.svmodes
```
