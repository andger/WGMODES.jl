# Code Validation

As the Julia code in this package is a relatively straight translation
from the original MATLAB code we expect it to function the same.
In order to ensure ensure that this package is functionally equivalent
we have added not only unit tests to test whether the functions run
without errors or warning, but also compare the results of the function.
To do so, we have run some example inputs through the original WGMODES
code running on [Octave](https://www.gnu.org/software/octave/) to get
the expected baseline results and add unit tests to check whether
the Julia code produces equivalent results.
The unit tests can be found within the `/test` directory.
We only compared the modal effective indices (not modal fields) using `isapprox` with
`rtol=1e-4`.

Validation results:

* `svmodes` unit tests consistently match expected `neff` results
* `wgmodes` unit tests *generally but not always* match with `rtol=1e-4`
* `wgmodes` results which did not match with `rtol=1e-4` would match with `rtol=1e-3`
* Some `wgmodes` results would match in values but not the order in which they were returned
