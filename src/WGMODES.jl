module WGMODES

import Arpack: eigs
import SparseArrays: sparse, I

include("utils.jl")
include("fully_vectorial_solver.jl")
include("semivectorial_solver.jl")

export wgmodes, svmodes

end
