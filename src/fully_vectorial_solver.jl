"""
    wgmodes(λ, guess, nmodes, dx, dy, varargin...)

This function computes the two transverse magnetic field components of a dielectric waveguide, using the finite difference method.

This function outputs `hx`, the three-dimensional vector containing Hx for each calculated mode, `hy`, the three-dimensional vector containing Hy for each calculated mode (e.g.: hy[:, k]=two dimensional Hy matrix for the k-th mode), `neff`, the vector of modal effective indices.

For details about the method, please consult: `A. B. Fallahkhair, K. S. Li and T. E. Murphy, "Vector Finite Difference Modesolver for Anisotropic Dielectric Waveguides", J. Lightwave Technol. 26[11], 1423-1431 (2008).`

NOTES:

1) The units are arbitrary, but they must be self-consistent (e.g., if λ is in μm, then dx and dy should also be in μm.)

2) Unlike the E-field modesolvers, this method calculates the transverse MAGNETIC field components Hx and Hy. Also it calculates the components at the edges [vertices] of each cell, rather than in the center of each cell. As a result, if size(eps)=[n, m], then the output eigenvectors will be have a size of [n+1, m+1].

3) This version of the modesolver can optionally support non-uniform grid sizes. To use this feature, you may let dx and/or dy be vectors instead of scalars.

4) The modesolver can consider anisotropic materials, provided the permittivity of all constituent materials can be expressed in one of the following forms:
[ε   0   0]  [εxx     0     0]	[εxx    εxy     0]
[0   ε   0]  [0     εyy     0]	[εyx    εyy     0]
[0   0   ε]  [0     0     εzz]	[0      0     εzz]
The program will decide which form is appropriate based upon the number of input arguments supplied.

5) Perfectly matched boundary layers can be accommodated by using the complex coordinate stretching technique at the edges of the computation window. (stretchmesh.m can be used for complex or real()-coordinate stretching.)

This is an unofficial Julia port of the matlab code by `Thomas E. Murphy [tem@umd.edu], Arman B. Fallahkhair [a.b.fallah@gmail.com], Kai Sum Li [ksl3@njit.edu]`. Much of the code and comments are copied from the original code. Please see `https://www.photonics.umd.edu/software/wgmodes/` for original code.

# Arguments
* `λ::F`: optical wavelength
* `guess::F`: scalar shift to apply when calculating the eigenvalues. This routine will return the eigenpairs which have an effective index closest to this guess
* `nmodes::FI`: the number of modes to calculate
* `dx::SV`: horizontal grid spacing [vector or scalar]
* `dy::SV`: vertical grid spacing [vector or scalar]
* `ε`: index mesh (isotropic materials) `OR`:
* `εxx, εxy, εyx, εyy, εzz`: index mesh (anisotropic)
* `boundary`: 4 letter string specifying boundary conditions to be applied at the edges of the computation window (`boundary[1]`=North boundary condition, `boundary[2]`=South boundary condition, `boundary[3]`=East boundary condition, `boundary[4]`=West boundary condition). The following boundary conditions are supported: `'A'`-Hx is antisymmetric, Hy is symmetric, `'S'`-Hx is symmetric and, Hy is antisymmetric, `'0'`-Hx and Hy are zero immediately outside of the boundary.

# Examples
```julia
[hx, hy, neff]=wgmodes(λ, guess, nmodes, dx, dy, ε, boundary)
[hx, hy, neff]=wgmodes(λ, guess, nmodes, dx, dy, εxx, εyy, εzz, boundary)
[hx, hy, neff]=wgmodes(λ, guess, nmodes, dx, dy, εxx, εxy, εyx, εyy, εzz, boundary)
```
"""
function wgmodes(
    λ::F,
    guess::F,
    nmodes::FI,
    dx::SV,
    dy::SV,
    varargin...,
) where {F<:AbstractFloat,FI<:Integer,SV<:Union{F,Array{F,1}}}
    if length(varargin) == 6
        epsxx, epsxy, epsyx, epsyy, epszz, boundary = varargin
    elseif length(varargin) == 4
        epsxx, epsyy, epszz, boundary = varargin
        epsxy = epsyx = zeros(size(epsxx))
    elseif length(varargin) == 2
        epsxx, boundary = varargin
        epsxy = epsyx = zeros(size(epsxx))
        epsyy = epszz = epsxx
    else
        error("Incorrect number of input arguments.\n")
    end

    nx, ny = size(epsxx) .+ 1
    dx = (length(dx) == 1) ? fill(dx, nx) : [dx[1]; dx; dx[end]]
    dy = (length(dy) == 1) ? fill(dy, ny) : [dy[1]; dy; dy[end]]

    _wgmodes(λ, guess, nmodes, dx, dy, epsxx, epsxy, epsyx, epsyy, epszz, boundary)
end

function _wgmodes(
    λ::F,
    guess::F,
    nmodes::FI,
    dx::Array{F,1},
    dy::Array{F,1},
    epsxx::Array{F,2},
    epsxy::Array{F,2},
    epsyx::Array{F,2},
    epsyy::Array{F,2},
    epszz::Array{F,2},
    boundary,
) where {F<:Number,FI<:Integer}

    @assert size(epsxx) ==
            size(epsxy) ==
            size(epsyx) ==
            size(epsyy) ==
            size(epszz) ==
            (length(dx) - 1, length(dy) - 1)

    nx, ny = size(epsxx) .+ 1

    dx = [dx[1]; dx; dx[end]]
    dy = [dy[1]; dy; dy[end]]

    # now we pad eps on all sides by one grid point
    epsxx, epsyy, epsxy, epsyx, epszz = pad2D.([epsxx, epsyy, epsxy, epsyx, epszz])

    k = 2π / λ # free-space wavevector

    # distance to neighboring points to north south east and west
    # relative to point under consideration [P], as shown below.

    n = (ones(nx)*dy[2:end-1]')[:]
    s = (ones(nx)*dy[1:end-2]')[:]
    e = (dx[2:end-1]*ones(ny)')[:]
    w = (dx[1:end-2]*ones(ny)')[:]

    # epsilon tensor elements in regions 1, 2, 3, 4, relative to the
    # mesh point under consideration [P], as shown below.
    #
    #								 NW-----N------NE
    #								 |		|		|
    #								 |	1	n	4	|
    #								 |		|		|
    #								 W--w---P---e---E
    #								 |		|		|
    #								 |	2	s	3	|
    #								 |		|		|
    #								 SW-----S------SE

    exx1, exx2, exx3, exx4 = nsew_eq(epsxx)
    eyy1, eyy2, eyy3, eyy4 = nsew_eq(epsyy)
    exy1, exy2, exy3, exy4 = nsew_eq(epsxy)
    eyx1, eyx2, eyx3, eyx4 = nsew_eq(epsyx)
    ezz1, ezz2, ezz3, ezz4 = nsew_eq(epszz)

    ns21 = @. n * eyy2 + s * eyy1
    ns34 = @. n * eyy3 + s * eyy4
    ew14 = @. e * exx1 + w * exx4
    ew23 = @. e * exx2 + w * exx3

    axxn = @. (
        (2 * eyy4 * e - eyx4 * n) * (eyy3 / ezz4) / ns34 +
        (2 * eyy1 * w + eyx1 * n) * (eyy2 / ezz1) / ns21
    ) / (n * (e + w))

    axxs = @. (
        (2 * eyy3 * e + eyx3 * s) * (eyy4 / ezz3) / ns34 +
        (2 * eyy2 * w - eyx2 * s) * (eyy1 / ezz2) / ns21
    ) / (s * (e + w))

    ayye = @. (2 * n * exx4 - e * exy4) * exx1 / ezz4 / e / ew14 / (n + s) +
       (2 * s * exx3 + e * exy3) * exx2 / ezz3 / e / ew23 / (n + s)

    ayyw = @. (2 * exx1 * n + exy1 * w) * exx4 / ezz1 / w / ew14 / (n + s) +
       (2 * exx2 * s - exy2 * w) * exx3 / ezz2 / w / ew23 / (n + s)

    axxe = @. 2 / (e * (e + w)) + (eyy4 * eyx3 / ezz3 - eyy3 * eyx4 / ezz4) / (e + w) / ns34

    axxw = @. 2 / (w * (e + w)) + (eyy2 * eyx1 / ezz1 - eyy1 * eyx2 / ezz2) / (e + w) / ns21

    ayyn = @. 2 / (n * (n + s)) + (exx4 * exy1 / ezz1 - exx1 * exy4 / ezz4) / (n + s) / ew14

    ayys = @. 2 / (s * (n + s)) + (exx2 * exy3 / ezz3 - exx3 * exy2 / ezz2) / (n + s) / ew23

    axxne = @. +eyx4 * eyy3 / ezz4 / (e + w) / ns34
    axxse = @. -eyx3 * eyy4 / ezz3 / (e + w) / ns34
    axxnw = @. -eyx1 * eyy2 / ezz1 / (e + w) / ns21
    axxsw = @. +eyx2 * eyy1 / ezz2 / (e + w) / ns21

    ayyne = @. +exy4 * exx1 / ezz4 / (n + s) / ew14
    ayyse = @. -exy3 * exx2 / ezz3 / (n + s) / ew23
    ayynw = @. -exy1 * exx4 / ezz1 / (n + s) / ew14
    ayysw = @. +exy2 * exx3 / ezz2 / (n + s) / ew23

    axxp = @. -axxn - axxs - axxe - axxw - axxne - axxse - axxnw - axxsw +
       k^2 * (n + s) * (eyy4 * eyy3 * e / ns34 + eyy1 * eyy2 * w / ns21) / (e + w)

    ayyp = @. -ayyn - ayys - ayye - ayyw - ayyne - ayyse - ayynw - ayysw +
       k^2 * (e + w) * (exx1 * exx4 * n / ew14 + exx2 * exx3 * s / ew23) / (n + s)

    axyn = @. (
        eyy3 * eyy4 / ezz4 / ns34 - eyy2 * eyy1 / ezz1 / ns21 +
        s * (eyy2 * eyy4 - eyy1 * eyy3) / ns21 / ns34
    ) / (e + w)

    axys = @. (
        eyy1 * eyy2 / ezz2 / ns21 - eyy4 * eyy3 / ezz3 / ns34 +
        n * (eyy2 * eyy4 - eyy1 * eyy3) / ns21 / ns34
    ) / (e + w)

    ayxe = @. (
        exx1 * exx4 / ezz4 / ew14 - exx2 * exx3 / ezz3 / ew23 +
        w * (exx2 * exx4 - exx1 * exx3) / ew23 / ew14
    ) / (n + s)

    ayxw = @. (
        exx3 * exx2 / ezz2 / ew23 - exx4 * exx1 / ezz1 / ew14 +
        e * (exx4 * exx2 - exx1 * exx3) / ew23 / ew14
    ) / (n + s)

    axye = @. (eyy4 * (1 - eyy3 ./ ezz3) - eyy3 * (1 - eyy4 / ezz4)) / ns34 / (e + w) -
       2 * (
        eyx1 * eyy2 / ezz1 * n * w / ns21 +
        eyx2 * eyy1 / ezz2 * s * w / ns21 +
        eyx4 * eyy3 / ezz4 * n * e / ns34 +
        eyx3 * eyy4 / ezz3 * s * e / ns34 +
        eyy1 * eyy2 * (1 / ezz1 - 1 / ezz2) * w^2 / ns21 +
        eyy3 * eyy4 * (1 / ezz4 - 1 / ezz3) * e * w / ns34
    ) / e / (e + w)^2

    axyw = @. (eyy2 * (1 - eyy1 / ezz1) - eyy1 * (1 - eyy2 / ezz2)) / ns21 / (e + w) .-
       2 * (
        eyx4 * eyy3 / ezz4 * n * e / ns34 +
        eyx3 * eyy4 / ezz3 * s * e / ns34 +
        eyx1 * eyy2 / ezz1 * n * w / ns21 +
        eyx2 * eyy1 / ezz2 * s * w / ns21 +
        eyy4 * eyy3 * (1 / ezz3 - 1 / ezz4) * e^2 / ns34 +
        eyy2 * eyy1 * (1 / ezz2 - 1 / ezz1) * w * e / ns21
    ) / w / (e + w)^2

    ayxn = @. (exx4 * (1 - exx1 / ezz1) - exx1 * (1 - exx4 / ezz4)) / ew14 / (n + s) -
       2 * (
        exy3 * exx2 / ezz3 * e * s / ew23 +
        exy2 * exx3 / ezz2 * w * s / ew23 +
        exy4 * exx1 / ezz4 * e * n / ew14 +
        exy1 * exx4 / ezz1 * w * n / ew14 +
        exx3 * exx2 * (1 / ezz3 - 1 / ezz2) * s^2 / ew23 +
        exx1 * exx4 * (1 / ezz4 - 1 / ezz1) * n * s / ew14
    ) / n / (n + s)^2

    ayxs = @. (exx2 * (1 - exx3 / ezz3) - exx3 * (1 - exx2 / ezz2)) / ew23 / (n + s) -
       2 * (
        exy4 * exx1 / ezz4 * e * n / ew14 +
        exy1 * exx4 / ezz1 * w * n / ew14 +
        exy3 * exx2 / ezz3 * e * s / ew23 +
        exy2 * exx3 / ezz2 * w * s / ew23 +
        exx4 * exx1 * (1 / ezz1 - 1 / ezz4) * n^2 / ew14 +
        exx2 * exx3 * (1 / ezz2 - 1 / ezz3) * s * n / ew23
    ) / s / (n + s)^2

    axyne = @. +eyy3 * (1 - eyy4 / ezz4) / (e + w) / ns34
    axyse = @. -eyy4 * (1 - eyy3 / ezz3) / (e + w) / ns34
    axynw = @. -eyy2 * (1 - eyy1 / ezz1) / (e + w) / ns21
    axysw = @. +eyy1 * (1 - eyy2 / ezz2) / (e + w) / ns21

    ayxne = @. +exx1 * (1 - exx4 / ezz4) / (n + s) / ew14
    ayxse = @. -exx2 * (1 - exx3 / ezz3) / (n + s) / ew23
    ayxnw = @. -exx4 * (1 - exx1 / ezz1) / (n + s) / ew14
    ayxsw = @. +exx3 * (1 - exx2 / ezz2) / (n + s) / ew23

    axyp = @. -(axyn + axys + axye + axyw + axyne + axyse + axynw + axysw) -
       k^2 * (
        w * (n * eyx1 * eyy2 + s * eyx2 * eyy1) / ns21 +
        e * (s * eyx3 * eyy4 + n * eyx4 * eyy3) / ns34
    ) / (e + w)

    ayxp = @. -(ayxn + ayxs + ayxe + ayxw + ayxne + ayxse + ayxnw + ayxsw) -
       k^2 * (
        n * (w * exy1 * exx4 + e * exy4 * exx1) / ew14 +
        s * (w * exy2 * exx3 + e * exy3 * exx2) / ew23
    ) / (n + s)

    ii = zeros(Int, nx, ny)
    ii[:] = 1:nx*ny

    # NORTH boundary
    ib = ii[:, end]
    if boundary[1] == 'S'
        sign = 1
    elseif boundary[1] == 'A'
        sign = -1
    elseif boundary[1] == '0'
        sign = 0
    else
        error("Unrecognized north boundary condition: " * boundary[1])
    end

    axxs[ib] = axxs[ib] + sign * axxn[ib]
    axxse[ib] = axxse[ib] + sign * axxne[ib]
    axxsw[ib] = axxsw[ib] + sign * axxnw[ib]
    ayxs[ib] = ayxs[ib] + sign * ayxn[ib]
    ayxse[ib] = ayxse[ib] + sign * ayxne[ib]
    ayxsw[ib] = ayxsw[ib] + sign * ayxnw[ib]
    ayys[ib] = ayys[ib] - sign * ayyn[ib]
    ayyse[ib] = ayyse[ib] - sign * ayyne[ib]
    ayysw[ib] = ayysw[ib] - sign * ayynw[ib]
    axys[ib] = axys[ib] - sign * axyn[ib]
    axyse[ib] = axyse[ib] - sign * axyne[ib]
    axysw[ib] = axysw[ib] - sign * axynw[ib]

    # SOUTH boundary
    ib = ii[:, 1]
    if boundary[2] == 'S'
        sign = 1
    elseif boundary[2] == 'A'
        sign = -1
    elseif boundary[2] == '0'
        sign = 0
    else
        error("Unrecognized south boundary condition: " * boundary[2])
    end

    axxn[ib] = axxn[ib] + sign * axxs[ib]
    axxne[ib] = axxne[ib] + sign * axxse[ib]
    axxnw[ib] = axxnw[ib] + sign * axxsw[ib]
    ayxn[ib] = ayxn[ib] + sign * ayxs[ib]
    ayxne[ib] = ayxne[ib] + sign * ayxse[ib]
    ayxnw[ib] = ayxnw[ib] + sign * ayxsw[ib]
    ayyn[ib] = ayyn[ib] - sign * ayys[ib]
    ayyne[ib] = ayyne[ib] - sign * ayyse[ib]
    ayynw[ib] = ayynw[ib] - sign * ayysw[ib]
    axyn[ib] = axyn[ib] - sign * axys[ib]
    axyne[ib] = axyne[ib] - sign * axyse[ib]
    axynw[ib] = axynw[ib] - sign * axysw[ib]

    # EAST boundary
    ib = ii[end, :]
    if boundary[3] == 'S'
        sign = 1
    elseif boundary[3] == 'A'
        sign = 1
    elseif boundary[3] == '0'
        sign = 0
    else
        error("Unrecognized east boundary condition: " * boundary[3])
    end

    axxw[ib] = axxw[ib] + sign * axxe[ib]
    axxnw[ib] = axxnw[ib] + sign * axxne[ib]
    axxsw[ib] = axxsw[ib] + sign * axxse[ib]
    ayxw[ib] = ayxw[ib] + sign * ayxe[ib]
    ayxnw[ib] = ayxnw[ib] + sign * ayxne[ib]
    ayxsw[ib] = ayxsw[ib] + sign * ayxse[ib]
    ayyw[ib] = ayyw[ib] - sign * ayye[ib]
    ayynw[ib] = ayynw[ib] - sign * ayyne[ib]
    ayysw[ib] = ayysw[ib] - sign * ayyse[ib]
    axyw[ib] = axyw[ib] - sign * axye[ib]
    axynw[ib] = axynw[ib] - sign * axyne[ib]
    axysw[ib] = axysw[ib] - sign * axyse[ib]

    # WEST boundary
    ib = ii[1, :]
    if boundary[4] == 'S'
        sign = 1
    elseif boundary[4] == 'A'
        sign = -1
    elseif boundary[4] == '0'
        sign = 0
    else
        error("Unrecognized west boundary condition: " * boundary[4])
    end

    axxe[ib] = axxe[ib] + sign * axxw[ib]
    axxne[ib] = axxne[ib] + sign * axxnw[ib]
    axxse[ib] = axxse[ib] + sign * axxsw[ib]
    ayxe[ib] = ayxe[ib] + sign * ayxw[ib]
    ayxne[ib] = ayxne[ib] + sign * ayxnw[ib]
    ayxse[ib] = ayxse[ib] + sign * ayxsw[ib]
    ayye[ib] = ayye[ib] - sign * ayyw[ib]
    ayyne[ib] = ayyne[ib] - sign * ayynw[ib]
    ayyse[ib] = ayyse[ib] - sign * ayysw[ib]
    axye[ib] = axye[ib] - sign * axyw[ib]
    axyne[ib] = axyne[ib] - sign * axynw[ib]
    axyse[ib] = axyse[ib] - sign * axysw[ib]

    # Assemble sparse matrix
    iall = ii[:]
    is = ii[:, 1:end-1][:]
    in = ii[:, 2:end][:]
    ie = ii[2:end, :][:]
    iw = ii[1:end-1, :][:]
    ine = ii[2:end, 2:end][:]
    ise = ii[2:end, 1:end-1][:]
    isw = ii[1:end-1, 1:end-1][:]
    inw = ii[1:end-1, 2:end][:]

    Axx = sparse(
        [iall; iw; ie; is; in; ine; ise; isw; inw],
        [iall; ie; iw; in; is; isw; inw; ine; ise],
        [
            axxp[iall]
            axxe[iw]
            axxw[ie]
            axxn[is]
            axxs[in]
            axxsw[ine]
            axxnw[ise]
            axxne[isw]
            axxse[inw]
        ],
    )

    Axy = sparse(
        [iall; iw; ie; is; in; ine; ise; isw; inw],
        [iall; ie; iw; in; is; isw; inw; ine; ise],
        [
            axyp[iall]
            axye[iw]
            axyw[ie]
            axyn[is]
            axys[in]
            axysw[ine]
            axynw[ise]
            axyne[isw]
            axyse[inw]
        ],
    )

    Ayx = sparse(
        [iall; iw; ie; is; in; ine; ise; isw; inw],
        [iall; ie; iw; in; is; isw; inw; ine; ise],
        [
            ayxp[iall]
            ayxe[iw]
            ayxw[ie]
            ayxn[is]
            ayxs[in]
            ayxsw[ine]
            ayxnw[ise]
            ayxne[isw]
            ayxse[inw]
        ],
    )

    Ayy = sparse(
        [iall; iw; ie; is; in; ine; ise; isw; inw],
        [iall; ie; iw; in; is; isw; inw; ine; ise],
        [
            ayyp[iall]
            ayye[iw]
            ayyw[ie]
            ayyn[is]
            ayys[in]
            ayysw[ine]
            ayynw[ise]
            ayyne[isw]
            ayyse[inw]
        ],
    )

    A = [[Axx Axy]; [Ayx Ayy]]

    shift = (guess * k)^2
    tol = 1e-8

    d, v = eigs(
        A,
        sparse(1.0 * I, size(A)[1], size(A)[1]),
        nev = nmodes,
        sigma = shift,
        tol = tol,
    )
    neff = λ * sqrt.(d) / (2π)

    phix = zeros(typeof(v[1]), nx, ny, nmodes)
    phiy = zeros(typeof(v[1]), nx, ny, nmodes)
    temp = zeros(typeof(v[1]), nx, 2 * ny)

    # Normalize modes

    temp = zeros(nx * ny, 2)
    for kk = 1:nmodes
        temp[:] = v[:, kk]
        mag, ii = findmax(sqrt.(sum(abs2.(temp), dims = 2)))
        ii = ii[1]
        jj = (abs(temp[ii, 1]) > abs(temp[ii, 2])) ? 1 : 2
        mag = mag * temp[ii, jj] / abs(temp[ii, jj])
        temp = temp / mag
        phix[:, :, kk] = reshape(temp[:, 1], nx, ny)
        phiy[:, :, kk] = reshape(temp[:, 2], nx, ny)
    end

    phix, phiy, neff
end
