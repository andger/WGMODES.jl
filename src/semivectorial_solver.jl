"""
    svmodes(lambda, guess, nmodes, dx, dy, epsilon, boundary, field)

This function calculates the modes of a dielectric waveguide using the
semivectorial finite difference method.

This function outputs `phi`, the three-dimensional vector containing the
requested field component for each computed mode, and `neff`, the vector
of modal effective indices.

This is an unofficial Julia port of the matlab code by
`Thomas E. Murphy [tem@umd.edu]`. Much of the code and comments are
copied from the original code. Please see
`https://www.photonics.umd.edu/software/wgmodes/` for original code.

# Arguments
* `lambda`: optical wavelength
* `guess`: scalar shift to apply when calculating the eigenvalues. This routine will return the eigenpairs which have an effective index closest to this guess
* `nmodes`: the number of modes to calculate
* `dx`: horizontal grid spacing
* `dy`: vertical grid spacing
* `epsilon`: index mesh (= n^2[x, y])
* `boundary`: 4 letter string specifying boundary conditions to be applied at the edges of the computation window (`boundary[1]`=North boundary condition, `boundary[2]`=South boundary condition, `boundary[3]`=East boundary condition, `boundary[4]`=West boundary condition). The following boundary conditions are supported: `'A'`-field is antisymmetric, `'S'`-field is symmetric, `'0'`-field is zero immediately outside of the boundary.
* `field`: must be `'EX'`, `'EY'`, or `'scalar'`
"""
function svmodes(lambda, guess, nmodes, dx, dy, epsilon, boundary, field)
    boundary = uppercase(boundary)
    if length(boundary) != 4 ||
       !occursin(string(boundary[1]), "AS0") ||
       !occursin(string(boundary[2]), "AS0") ||
       !occursin(string(boundary[3]), "AS0") ||
       !occursin(string(boundary[4]), "AS0")
        error("Bad boundary")
    end
    field = lowercase(field)
    if field != "ex" && field != "ey" && field != "scalar"
        error("Bad field")
    end

    nx, ny = size(epsilon)

    # now we pad epsilon on all sides by one grid point
    epsilon = pad2D(epsilon)

    # compute free-space wavevector
    k = 2 * pi / lambda

    dx = (length(dx) == 1) ? fill(dx, nx + 2) : [dx[1]; dx; dx[end]]
    dy = (length(dy) == 1) ? fill(dy, ny + 2) : [dy[1]; dy; dy[end]]

    n = (ones(nx)*(dy[3:end] + dy[2:end-1])'/2)[:]
    s = (ones(nx)*(dy[1:end-2] + dy[2:end-1])'/2)[:]
    e = ((dx[3:end]+dx[2:end-1])/2*ones(ny)')[:]
    w = ((dx[1:end-2]+dx[2:end-1])/2*ones(ny)')[:]
    p = (dx[2:end-1]*ones(ny)')[:]
    q = (ones(nx)*dy[2:end-1]')[:]

    en, es, ee, ew, ep = nsewp(epsilon)

    if field == "ex"
        an = 2 ./ n ./ (n + s)
        as = 2 ./ s ./ (n + s)
        tmp = adenom(p, ep, ee, e, ew, w)
        ae = 8 * (p .* (ep - ew) + 2 .* w .* ew) .* ee ./ tmp
        aw = 8 * (p .* (ep - ee) + 2 .* e .* ee) .* ew ./ tmp
        ap = ep .* k^2 - an - as - ae .* ep ./ ee - aw .* ep ./ ew
    elseif field == "ey"
        tmp = adenom(q, ep, en, n, es, s)
        an = 8 * (q .* (ep - es) + 2 .* s .* es) .* en ./ tmp
        as = 8 * (q .* (ep - en) + 2 .* n .* en) .* es ./ tmp
        ae = 2 ./ e ./ (e + w)
        aw = 2 ./ w ./ (e + w)
        ap = ep .* k^2 - an .* ep ./ en - as .* ep ./ es - ae - aw
    elseif field == "scalar"
        an = 2 ./ n ./ (n + s)
        as = 2 ./ s ./ (n + s)
        ae = 2 ./ e ./ (e + w)
        aw = 2 ./ w ./ (e + w)
        ap = ep .* k^2 - an - as - ae - aw
    else
        error("Bad field")
    end

    ii = zeros(Int, nx, ny)
    ii[:] = 1:nx*ny

    # Modify matrix elements to account for boundary conditions

    # north boundary
    ib = ii[:, end]
    if boundary[1] == 'S'
        ap[ib] = ap[ib] + an[ib]
    elseif boundary[1] == 'A'
        ap[ib] = ap[ib] - an[ib]
    end

    # south boundary
    ib = ii[:, 1]
    if boundary[2] == 'S'
        ap[ib] = ap[ib] + as[ib]
    elseif boundary[2] == 'A'
        ap[ib] = ap[ib] - as[ib]
    end

    # east boundary
    ib = ii[end, :]
    if boundary[3] == 'S'
        ap[ib] = ap[ib] + ae[ib]
    elseif boundary[3] == 'A'
        ap[ib] = ap[ib] - ae[ib]
    end

    # west boundary
    ib = ii[1, :]
    if boundary[4] == 'S'
        ap[ib] = ap[ib] + aw[ib]
    elseif boundary[4] == 'A'
        ap[ib] = ap[ib] - aw[ib]
    end

    iall = ii[:]
    in = ii[:, 2:end][:]
    is = ii[:, 1:end-1][:]
    ie = ii[2:end, :][:]
    iw = ii[1:end-1, :][:]

    i = [iall; iw; ie; is; in]
    j = [iall; ie; iw; in; is]
    v = [ap[iall]; ae[iw]; aw[ie]; an[is]; as[in]]
    A = sparse(i, j, v)

    shift = (2 * pi * guess / lambda)^2
    tol = 1e-8

    d, v = eigs(
        A,
        sparse(1.0 * I, size(A)[1], size(A)[1]),
        nev = nmodes,
        sigma = shift,
        tol = tol,
    )
    neff = lambda * sqrt.(complex.(d)) / (2 * pi)

    phi = zeros(typeof(v[1]), nx, ny, nmodes)
    temp = zeros(typeof(v[1]), nx, ny)

    for k = 1:nmodes
        temp[:] = v[:, k] / maximum(abs.(v[:, k]))
        phi[:, :, k] = temp
    end

    phi, neff
end
