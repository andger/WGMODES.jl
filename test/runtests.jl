import WGMODES
import Test: @test_broken, @test, @test_nowarn, @test_throws

tests = ["svmodes", "wgmodes"]

#TODO: rtol set to 1e-4 by default. Is this acceptable?
approxeq(a, b; rtol = 1e-4) = all(isapprox.(a, b, rtol = rtol))

for t in tests
    @info "Running " * t * ".jl"
    include("$(t).jl")
    @info "Finished " * t * ".jl"
end
