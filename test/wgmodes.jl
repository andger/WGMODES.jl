lambda = 1.0
guess = 3.0
nmodes = 5
dx = 1.0
dy = 1.0
epsilon = ones(10, 10)
epsilon[4:7, 4:7] *= 3
@test_nowarn phix, phiy, neff =
    WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, epsilon, "0000")
@test_nowarn phix, phiy, neff =
    WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, epsilon, "AAAA")
@test_nowarn phix, phiy, neff =
    WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, epsilon, "SSSS")

GC.gc()

@test_nowarn phix, phiy, neff =
    WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, epsilon, epsilon, epsilon, "0000")
@test_nowarn phix, phiy, neff = WGMODES.wgmodes(
    lambda,
    guess,
    nmodes,
    dx,
    dy,
    epsilon,
    epsilon,
    epsilon,
    epsilon,
    epsilon,
    "0000",
)

GC.gc()

@test_throws Exception WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, epsilon, "XXXXX")
@test_throws Exception WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, epsilon, "XXXX")

GC.gc()

# Simple test comparing to values obtained from original WGMODES code as
# run on Octave
lambda = 1.0
dx = dy = 0.01
n = ones(200, 300)
n[50:100, 100:200] *= 3
eps = n .^ 2
guess = 3.0
nmodes = 6
boundary = "0000"
hx, hy, neff = WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, eps, boundary)
@test approxeq(neff, [2.8491, 2.8085, 2.7203, 2.7006, 2.5143, 2.4966])

GC.gc()

lambda = 1.0
dx = dy = 0.01
n = ones(200, 300)
n[1:99, 1:300] .= 1.5
n[100:110, 1:300] .= 3
n[100:150, 100:200] .= 3
eps = n .^ 2
guess = 3.0
nmodes = 6
boundary = "0000"
hx, hy, neff = WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, eps, boundary)
@test approxeq(neff, [2.8522, 2.8148, 2.7281, 2.7086, 2.5247, 2.5217])

GC.gc()

lambda = 1.5
dx = dy = 0.01
n = ones(200, 300)
n[1:99, 1:300] .= 1.5
n[100:110, 1:300] .= 3
n[100:150, 100:200] .= 3
eps = n .^ 2
guess = 3.0
nmodes = 6
boundary = "0000"
hx, hy, neff = WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, eps, boundary)
@test approxeq(neff, [2.7047, 2.5936, 2.4172, 2.3591, 2.0533, 1.9432])

GC.gc()

lambda = 1.5
dx = 0.005
dy = 0.01
n = ones(200, 300)
n[1:99, 1:300] .= 1.5
n[100:110, 1:300] .= 3
n[100:150, 100:200] .= 3
eps = n .^ 2
guess = 3.0
nmodes = 6
boundary = "0000"
hx, hy, neff = WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, eps, boundary)
@test approxeq(neff, [2.3778, 2.0591, 1.7682, 1.6242, 1.4327, 1.3988])

GC.gc()

lambda = 2.0
dx = dy = 0.01
n = ones(200, 300)
n[1:99, 1:300] .= 1.5
n[100:110, 1:300] .= 3
n[100:150, 100:200] .= 3
eps = n .^ 2
guess = 3.0
nmodes = 6
boundary = "AAAA"
hx, hy, neff = WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, eps, boundary)
#TODO: Results are slightly off
@test_broken approxeq(neff, [2.5261, 2.2953, 2.0149, 1.8876, 1.6627, 1.4984], rtol = 1e-4)
@test approxeq(neff, [2.5261, 2.2953, 2.0149, 1.8876, 1.6627, 1.4984], rtol = 1e-3)

GC.gc()

lambda = 2.0
dx = dy = 0.01
n = ones(200, 300)
n[1:99, 1:300] .= 1.5
n[100:110, 1:300] .= 3
n[100:150, 100:200] .= 3
eps = n .^ 2
guess = 3.0
nmodes = 6
boundary = "SSSS"
hx, hy, neff = WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, eps, boundary)
@test approxeq(neff, [2.5263, 2.2952, 2.0156, 1.8873, 1.7182, 1.6634])

GC.gc()

lambda = 2.0
dx = dy = 0.01
n = ones(200, 300)
n[1:99, 1:300] .= 1.5
n[100:110, 1:300] .= 3
n[100:150, 100:200] .= 3
eps = n .^ 2
guess = 3.0
nmodes = 4
boundary = "0000"
hx, hy, neff = WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, eps, boundary)
@test approxeq(neff, [2.5262, 2.2952, 2.0148, 1.8871])

GC.gc()

lambda = 2.0
dx = ones(200) / 100
dx[1:5:200] .*= 2
dy = ones(300) / 100
dy[1:10:300] .*= 1.5
n = ones(200, 300)
n[1:99, 1:300] .= 1.5
n[100:110, 1:300] .= 3
n[100:150, 100:200] .= 3
eps = n .^ 2
guess = 3.0
nmodes = 4
boundary = "0000"
#TODO: Handle case where `dx`, `dy` are same length as `eps` boundaries? Or is this correct?
@test_broken WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, eps, boundary)

GC.gc()

lambda = 2.0
dx = ones(199) / 100
dx[1:5:199] .*= 2
dy = ones(299) / 100
dy[1:10:299] .*= 1.5
n = ones(200, 300)
n[1:99, 1:300] .= 1.5
n[100:110, 1:300] .= 3
n[100:150, 100:200] .= 3
eps = n .^ 2
guess = 3.0
nmodes = 4
boundary = "0000"
hx, hy, neff = WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, eps, boundary)
#TODO: Results are slightly off
@test_broken approxeq(neff, [2.6128, 2.4759, 2.1534, 2.1143], rtol = 1e-4)
@test approxeq(neff, [2.6128, 2.4759, 2.1534, 2.1143], rtol = 1e-3)

GC.gc()

lambda = 2.0
dx = dy = 0.01
n = ones(200, 300)
n[1:99, 1:300] .= 1.5
n[100:110, 1:300] .= 3
n[100:150, 100:200] .= 3
eps = n .^ 2
guess = 2.0
nmodes = 4
boundary = "0000"
hx, hy, neff = WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, eps, boundary)
#TODO: Same approximate results, but different ordering
@test_broken approxeq(neff, [2.0148, 1.8871, 2.2952, 1.6635])
@test approxeq(neff, [2.0148, 1.8871, 1.6635, 2.2952])

GC.gc()

lambda = 2.0
dx = dy = 0.01
n = ones(200, 300)
n[1:99, 1:300] .= 1.5
n[100:110, 1:300] .= 3
n[100:150, 100:200] .= 3
eps = n .^ 2
guess = 3.0
nmodes = 4
boundary = "0000"
hx, hy, neff = WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, eps, eps, eps, boundary)
@test approxeq(neff, [2.5262, 2.2953, 2.0148, 1.8871])

GC.gc()

lambda = 2.0
dx = dy = 0.01
n = ones(200, 300)
n[1:99, 1:300] .= 1.5
n[100:110, 1:300] .= 3
n[100:150, 100:200] .= 3
eps = n .^ 2
guess = 3.0
nmodes = 4
boundary = "0000"
hx, hy, neff =
    WGMODES.wgmodes(lambda, guess, nmodes, dx, dy, eps, eps, eps, eps, eps, boundary)
#TODO: Same approximate results, but different ordering
@test_broken approxeq(neff, [3.1887, 3.6296, 2.4500, 2.1643])
@test approxeq(neff, [3.1887, 2.4500, 3.6296, 2.1643])
